function Human(name) {
  this._name = name;
}

Human.prototype.getName = function () {
  return this._name;
};

function Developer(name, languages) {
  Human.call(this, name);
  this._programmingLanguages = languages || [];
}

Developer.prototype = new Human();

Developer.prototype.getLanguages = function () {
  return this._programmingLanguages;
};

Developer.prototype.toString = function () {
  var result = this._name + ', knows: ';
  if (!this._programmingLanguages.length) {
    result += 'nothing';
    return result;
  }
  this._programmingLanguages.forEach(function (l) {
    result += l + ', ';
  });
  return result.substring(0, result.length - 2);
};


exports.Human = Human;
exports.Developer = Developer;
