var human = {
  _name: null,
  getName: function () {
    return this._name;
  },
  init: function (name) {
    this._name = name;
    return this;
  }
};

var developer = Object.create(human);
developer.init = function (name, languages) {
  Object.getPrototypeOf(this).init(name);
  this._programmingLanguages = languages || [];
  return this;
};

developer.getLanguages = function () {
  return this._programmingLanguages;
};

developer.toString = function () {
  var result = this._name + ', knows: ';
  if (!this._programmingLanguages.length) {
    result += 'nothing';
    return result;
  }
  this._programmingLanguages.forEach(function (l) {
    result += l + ', ';
  });
  return result.substring(0, result.length - 2);
};

exports.human = human;
exports.developer = developer;