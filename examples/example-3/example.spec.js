var task = require('./task'),
    human = task.human,
    dev = task.developer;

describe('human', function () {
  it('should be an object', function () {
    expect(typeof human).toBe('object');
  });
  it('should initialize the property _name', function () {
    human.init('foo');
    expect(human._name).toBe('foo');
  });
  it('should has method getName', function () {
    expect(typeof human.getName).toBe('function');
  });
  it('should return name when getName is called', function () {
    expect(human._name === human.getName()).toBeTruthy();
  });
});

describe('developer', function () {
  it('should inherit from human', function () {
    expect(Object.getPrototypeOf(dev)).toBe(human);
  });
  it('should initialize all its properties when init is called', function () {
    var langs = ['JavaScript'];
    dev.init('foo', langs);
    expect(dev.getName()).toBe('foo');
    expect(dev.getLanguages()).toBe(langs);
  });
  it('should call the init method of its parent', function () {
    dev.init('bar');
    expect(Object.getPrototypeOf(dev)._name).toBe('bar');
  });
  it('should return correct string representation', function () {
    expect(dev.init('foo', ['js', 'java']).toString()).toBe('foo, knows: js, java');
    expect(dev.init('foo').toString()).toBe('foo, knows: nothing');
  });
});