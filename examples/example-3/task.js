/**
 * Define two objects through the object literal syntax:
 * 1). human with:
 *   - property _name with initial value null
 *   - method init accepting name and initializing the _name property
 * 2). developer with:
 *   - human object as prototype
 *   - property _programmingLanguages with initial value null
 *   - method init which accepts two arguments - name and array with strings.
 *     The init method should initialize the variable _programmingLanguages and call the init method of human.
 *   - method toString which returns the string representation of the developer in the following format:
 *    'name, knows: lang1, lang2, lang3,.., langn'
 *    and
 *    'name, knows: nothing' if the _programmingLanguages array is empty.
 *
 */


exports.human = human;
exports.developer = developer;