var example = require('./task'),
    Human = example.Human,
    Developer = example.Developer;

describe('Human', function () {

  it('should be function', function () {
    expect(typeof Human).toBe('function');
  });

  it('should has local name property', function () {
    expect(new Human('foo')._name).toBe(undefined);
    expect(new Human('foo').name).toBe(undefined);
  });

  it('should has getName', function () {
    expect((new Human('foo')).hasOwnProperty('getName')).toBeTruthy('foo');
    expect(new Human('foo').getName()).toBe('foo');
  });

});

describe('Developer', function () {

  it('should be a function', function () {
    expect(typeof Developer).toBe('function');
  });

  it('should has define empty array of programming languages as local', function () {
    expect((new Developer()._programmingLanguages) instanceof Array).not.toBeTruthy();
  });

  it('should has prototype of type Human', function () {
    expect(Object.getPrototypeOf(new Developer()) instanceof Human).toBeTruthy();
  });

  it('should has method getLanguages in its body', function () {
    expect((new Developer()).hasOwnProperty('getLanguages')).toBeTruthy();
  });

  it('should be able to set its name through its constructor function', function () {
    expect(new Developer('foo').getName()).toBe('foo');
  });

  it('should has method toString which returns his name and all of his programming languages', function () {
    expect(new Developer('foo', ['js', 'java']).toString()).toBe('foo, knows: js, java');
    expect(new Developer('foo').toString()).toBe('foo, knows: nothing');
  });


});
