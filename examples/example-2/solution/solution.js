function Human(humanName) {

  var name = humanName;

  this.getName = function () {
    return name;
  };
}

function Developer(humanName, languages) {

  Human.call(this, humanName);

  var programmingLanguages = languages || [];

  this.getLanguages = function () {
    return programmingLanguages;
  };

  this.toString = function () {
    var result = this.getName() + ', knows: ';
    if (!programmingLanguages.length) {
      result += 'nothing';
      return result;
    }
    programmingLanguages.forEach(function (l) {
      result += l + ', ';
    });
    return result.substring(0, result.length - 2);
  };

}

Developer.prototype = new Human();

exports.Human = Human;
exports.Developer = Developer;
