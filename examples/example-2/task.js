/*
 * ## Closure inheritance
 *
 * You should define two constructor functions:
 * 1) Human
 *  - accepting a single argument and setting the _name property
 *  - it should has a getName method in its prototype which returns the property _name.
 * 2) Developer
 *  - accepting two arguments and setting developer's name and programming languages he knows (array of programming languages)
 *  - method getLanguages which returns the array _programmingLanguages
 *  - method toString which returns the string representation of the developer in the following format:
 *    'name, knows: lang1, lang2, lang3,.., langn'
 *    and
 *    'name, knows: nothing' if the _programmingLanguages array is empty.
 */

exports.Human = Human;
exports.Developer = Developer;
