/**
 * @author Minko Gechev (@mgechev)
 * @return {object} The public interface of the core.
 */
exports.core = (function () {

    var modules = {};

    /* * * * * * * * * *  Initializing the modules' sandbox * * * * * * * * * */

    /**
     * Publish subscribe implementation which is providing communication
     * and decoupling between the modules.
     *
     * @return {object} Public interface of the publish/subscribe
     */
    var pubSub = (function () {
        var topics = {};

        /**
         * This method is subscribing a module to an event with given callback.
         *
         * @public
         * @param {string} event The event name.
         * @param {function} callback The callback which will be called when the event occure.
         */
        function subscribe(event, callback) {
            var subs = topics[event];
            if (subs === undefined) {
                subs = [];
            }
            subs.push(callback);
            topics[event] = subs;
        }

        /**
         * Method which is publishing different events with specified arguments.
         *
         * @public
         * @param {string} event Event name.
         * @param {array} args Arguments array.
         * @return {boolean} true/false depending on whether any callback is called.
         */
        function publish(event, args) {
            var callbacks = topics[event];
            if (callbacks !== undefined) {
                for (var i = 0; i < callbacks.length; i += 1) {
                    callbacks[i].call(null, args);
                }
                return true;
            }
            console.log('No subscribtions for ' + event);
            return false;
        }
        return {
            publish: publish,
            subscribe: subscribe
        };
    }());

    var sandbox = pubSub;

    /* * * * * * * * * End of the sandbox initialization * * * * * * * * */


    /* * * * * *  Initializing the static methods of plainvm * * * * * * */

    /**
     * Registering a module. This method is only putting the module into the hashmap
     * containig all modules.
     *
     * @static
     * @public
     * @param {string} name Module name.
     * @param {object} module The module.
     */
    function register(name, module) {
        if (modules[name] !== undefined) {
            console.log('Module "' + name + '" already exists!');
        }
        modules[name] = module;
    }

    /**
     * Starting a specific module. This module is calling the init method of the specified module.
     *
     * @static
     * @public
     * @param {string} name The module name
     * @return {boolean} true/false depending on whether the init method have been called.
     */
    function start(name) {
        var module = modules[name];
        if (module === undefined) {
            console.log('Module "' + name + '" does not exists!');
            return false;
        }
        if (typeof module.init === 'function') {
            module.init.apply(module, [sandbox].concat(Array.prototype.slice.call(arguments, 1, arguments.length)));
            return true;
        } else {
            console.log('Cannot init the module "' + name + '" because there\'s not init method');
            return false;
        }
    }

    /**
     * Unregister removes initialized module. It's also calling it's detach method.
     *
     * @static
     * @public
     * @param {string} name The module's name
     * @return {boolean} true/false depends whether the detach module have been called.
     */
    function unregister(name) {
        var module = modules[name],
            result = false;
        if (module !== undefined) {
            if (typeof module.detach === 'function') {
                module.detach();
                result = true;
            } else {
                console.log('Cannot detach "' + name + '" detach method is missing');
            }
            delete modules[name];
            return result;
        }
        console.log('Cannot remove module "' + name + '" because it\'s not registered');
        return false;
    }

    /* * * * * * * * * * * The initialization of the static method is finished  * * * * * * * * * */

    return {
        register: register,
        start: start,
        unregister: unregister
    };

}());