exports.writeFile = function (file, content, callback) {
  if (file === 'error') {
    callback('Error');
  } else {
    callback();
  }
};

exports.readFile = function (file, callback) {
  if (file === 'error') {
    callback('Error');
  } else {
    callback(undefined, 'Content');
  }
};

exports.unlink = function (file, callback) {
  if (file === 'error') {
    callback('Error');
  } else {
    callback();
  }
};