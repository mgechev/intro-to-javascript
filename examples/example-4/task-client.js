/**
 * Add the proper event handlers and fire the proper events to make the server and client work together
 */

var core = require('./lib/core').core;

var client = (function () {
  var sandbox, socket;

  function addEventHandlers() {
    socket.on('data', function (data) {
      data = JSON.parse(data);
      var type = data.type === 'error' ? 'Error' : 'Success',
          message = data.content;
      console.log(type + ', ' + message + '.');
      console.log('\n');
      sandbox.publish('ui.show-menu');
    });
    socket.on('end', function () {
      console.log('Disconnected');
      socket.destroy();
      process.exit(1);
    });
  }

  function handleCommand(command) {
    socket.write(command);
  }

  return {
    init: function (sndbox, netLib, host, port) {
      this.sandbox = sandbox = sndbox;
      socket = require(netLib).connect(port, host, function (s) {
        console.log('Connected to ' + host + ':' + port);
        addEventHandlers();
        sandbox.publish('ui.show-menu');
        sandbox.subscribe('ui.command', handleCommand);
      });
    }
  };
}());

var ui = (function () {
  var sandbox;

  function showMenu() {
    var readline = require('readline');

    var rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout
    });

    console.log('\n------------------------------------------------------');
    console.log('| To read file:\t\t read filename');
    console.log('| To write file:\t write filename file content');
    console.log('| To delete file:\t delete filename');
    console.log('| To disconnect:\t Ctrl+C');
    console.log('------------------------------------------------------\n');
    rl.question('Enter command: ', function (input) {
      console.log('\n');
      sandbox.publish('ui.command', input);
      rl.close();
    });
  }

  return {
    init: function (sndbox) {
      this.sandbox = sandbox = sndbox;
      sandbox.subscribe('ui.show-menu', showMenu);
    }
  };
}());

core.register('client', client);
core.register('ui', ui);

exports.client = core;