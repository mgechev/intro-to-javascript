/**
 * Add the proper event handlers and fire the proper events to make the server and client work together
 */

var core = require('./lib/core').core;

var server = (function () {

  var sandbox;

  function handleClient(socket) {
    socket.on('data', function (data) {
      var command = data.toString();
      sandbox.publish('command.handle', {
        command: command.trim(),
        socket: socket
      });
    });
    socket.on('end', function () {
      socket.destroy();
    });
  }

  function response(data) {
    var rsp = {
      type: data.type,
      content: data.content
    };
    data.socket.write(JSON.stringify(rsp));
  }

  return {
    init: function (sndbox, netLib, port) {
      sandbox = sndbox;
      sandbox.subscribe('command.result', function (data) {
        response(data);
      });
      var net = require(netLib);
      net.createServer(handleClient).listen(port);
    }
  };
}());

var commandHandler = (function () {
  var fileSystem,
      sandbox;

  function parseCommand(data) {
    var parts = data.split(' ');
    return {
      command: parts[0],
      args: parts.slice(1, parts.length)
    };
  }

  function handleCommand(data) {
    if (!data) return;
    var command = parseCommand(data.command),
        socket = data.socket,
        success = function (data) { response('success', data, socket); },
        error = function (data) { response('error', data, socket); };
    switch (command.command) {
      case 'write':
        writeFile(command.args[0], command.args.slice(1, command.args.length).join(' '), success, error);
        break;
      case 'read':
        readFile(command.args[0], success, error);
        break;
      case 'delete': deleteFile(command.args[0], success, error);
        break;
      default: error('Unknown command');
    }
  }

  function writeFile(file, content, success, errorHandler) {
    fileSystem.writeFile(file, content, function (error) {
      if (error) {
        errorHandler(JSON.stringify(error));
      } else {
        success('The file ' + file + ' content was successfully wrote');
      }
    });
  }

  function readFile(file, success, errorHandler) {
    fileSystem.readFile(file, function (error, content) {
      if (error) {
        errorHandler(JSON.stringify(error));
      } else {
        success(content.toString());
      }
    });
  }

  function deleteFile(file, success, errorHandler) {
    fileSystem.unlink(file, function (error) {
      if (error) errorHandler(JSON.stringify(error));
      else success('The file is successfully deleted');
    });
  }

  function response(type, content, socket) {
    sandbox.publish('command.result', {
      type: type,
      content: content,
      socket: socket
    });
  }

  return {
    init: function (sndbox, fileSystemLib) {
      sandbox = sndbox;
      fileSystem = require('fs');
      sandbox.subscribe('command.handle', handleCommand);
    }
  };
}());

core.register('server', server);
core.register('command-handler', commandHandler);

exports.server = core;